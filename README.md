## Local Development Environment Setup ##

1. Install Node.js
2. Clone the React Native App repository
3. Run ```npm i``` at the root of your local project folder
4. To run on iOS: run XCode and open ios folder of your local project folder. Select needed device and run
5. To run on Anroid: run Android emulator / or device and execute ```react-native run-android```
